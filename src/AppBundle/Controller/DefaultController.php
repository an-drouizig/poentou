<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Points;
use AppBundle\Type\PointsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        // replace this example code with whatever you need
        $form = $this->createForm(PointsType::class, null);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
           /** @var Points */
  			   $points = $form->getData();

  			   $this->getDoctrine()->getManager()->persist($points);
  			   $this->getDoctrine()->getManager()->flush();
  			   return $this->redirectToRoute('homepage');
  		  }

        $points = $this->getAllPoints();

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'points' => $points,
        ]);
    }


    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {
        $basePoints = null;
        if($request->get('id')) {
          $basePoints =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Points')->find($request->get('id'));
        }

        $form = $this->createForm(PointsType::class, $basePoints);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
           /** @var Points */
  			   $points = $form->getData();

  			   $this->getDoctrine()->getManager()->persist($points);
  			   $this->getDoctrine()->getManager()->flush();
  			   return $this->redirectToRoute('admin');
  		  }

        $points = $this->getAllPoints();

        return $this->render('default/admin.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'points' => $points,
        ]);
    }
    /**
     * @Route("/remove/{id}", name="remove")
     * @ParamConverter("points", class="AppBundle:Points")
     */
    public function removeAction(Request $request, Points $points)
    {

  	   $this->getDoctrine()->getManager()->remove($points);
  	   $this->getDoctrine()->getManager()->flush();
       return $this->redirectToRoute('admin');
    }

    protected function getAllPoints()
    {
      $pointsRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Points');
      $query = $pointsRepo->createQueryBuilder('p')
        ->orderBy('p.points', 'DESC')
        ->getQuery();

      return $query->execute();
    }
}
