<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="points")
*/
class Points
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;
	
	/**
	* @ORM\Column(type="string", length=255)
	*/
    protected $name;
    
    /**
	* @ORM\Column(type="string", length=255)
	*/
    protected $email;
    
    /**
	* @ORM\Column(type="integer")
	*/
    protected $points;
    
    public function getId()
    {
		return $this->id;
	}
	
	public function setId($value)
	{
		$this->id = $value;
	}
    
    public function getName()
    {
		return $this->name;
	}
	
	public function setName($value)
	{
		$this->name = $value;
	}
    
    public function getEmail()
    {
		return $this->email;
	}
	
	public function setEmail($value)
	{
		$this->email = $value;
	}
    
    public function getPoints()
    {
		return $this->points;
	}
	
	public function setPoints($value)
	{
		$this->points = $value;
	}
}
