<?php
// src/AppBundle/Form/TaskType.php
namespace AppBundle\Type	;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PointsType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', null, [
				'label' => 'poentou.form.name.label'
			])
			->add('email', EmailType::class, [
				'label' => 'poentou.form.email.label'
			])
			->add('points', null, [
				'label' => 'poentou.form.points.label'
			])
			->add('save', SubmitType::class, [
				'label' => 'poentou.form.save.label'
			])
			;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\Points',
		));
	}
}
